package com.manasa.inventoryservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.manasa.inventoryservice.repository.InventoryEntity;
import com.manasa.inventoryservice.service.InventoryService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/inventory")
public class InventoryController {

	@Autowired
	private InventoryService inventoryService;

	/**
	 * 
	 * @return expose GET endpoint to return {@link List} of all available
	 *         inventory items
	 */
	@GetMapping
	public List<Inventory> getAllInventories() {
		return inventoryService.getAllInventories();
	}

	/**
	 * 
	 * @param inventoryId
	 *            supplied as path variable
	 * @return expose GET endpoint to return {@link Inventory} for the
	 *         supplied inventory id return HTTP 404 in case inventory is not found
	 *         in database
	 */
	@GetMapping(value = "/{inventoryId}")
	public ResponseEntity<Inventory> getInventory(@PathVariable("inventoryId") Long inventoryId) {
		return inventoryService.getInventoryById(inventoryId).map(inventory -> {
			return ResponseEntity.ok(inventory);
		}).orElseGet(() -> {
			return new ResponseEntity<Inventory>(HttpStatus.NOT_FOUND);
		});
	}

	/**
	 * 
	 * @param inventory
	 *            JSON body
	 * @return expose POST mapping and return newly created inventory in case of
	 *         successful operation return HTTP 417 in case of failure
	 */
	@PostMapping
	public ResponseEntity<Inventory> addNewInventory(@RequestBody Inventory inventory) {
		return inventoryService.saveUpdateInventory(inventory).map(i -> {
			return ResponseEntity.ok(i);
		}).orElseGet(() -> {
			return new ResponseEntity<Inventory>(HttpStatus.EXPECTATION_FAILED);
		});
	}

	/**
	 * 
	 * @param inventory
	 *            JSON body
	 * @return expose PUT mapping and return newly created or updated inventory in
	 *         case of successful operation return HTTP 417 in case of failure
	 * 
	 */
	@PutMapping
	public ResponseEntity<Inventory> updateInventory(@RequestBody Inventory inventory) {
		return inventoryService.saveUpdateInventory(inventory).map(i -> {
			return ResponseEntity.ok(i);
		}).orElseGet(() -> {
			return new ResponseEntity<Inventory>(HttpStatus.EXPECTATION_FAILED);
		});
	}

	/**
	 * 
	 * @param inventoryId
	 *            inventory id to be deleted
	 * @return expose DELETE mapping and return success message if operation was
	 *         successful. return HTTP 417 in case of failure
	 * 
	 */
	@DeleteMapping(value = "/{inventoryId}")
	public ResponseEntity<String> deleteInventoryEntity(@PathVariable("inventoryId") Long inventoryId) {
		if (inventoryService.removeInventory(inventoryId)) {
			return ResponseEntity.ok("InventoryEntity with id : " + inventoryId + " removed");
		} else {
			return new ResponseEntity<String>("Error deleting enitty ", HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	/**
	 * 
	 * @param productCode
	 *            supplied as path variable
	 * @return expose GET endpoint to return {@link InventoryEntity} for the
	 *         supplied product code return HTTP 404 in case inventory is not found
	 *         in database
	 */
	@GetMapping(value = "/filter")
	public ResponseEntity<Inventory> findInventoryByProductCode(@RequestParam("productCode") String productCode) {
		log.info("Finding inventory for product code :" + productCode);
		return inventoryService.findByProductCode(productCode);
	}
}
