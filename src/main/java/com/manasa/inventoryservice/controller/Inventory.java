package com.manasa.inventoryservice.controller;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Inventory {

	private Long id;
	private String productCode;
	private Integer availableQuantity;

	public Inventory(Long id, String productCode, Integer availableQuantity) {
		this.id = id;
		this.productCode = productCode;
		this.availableQuantity = availableQuantity;
	}
}
