package com.manasa.inventoryservice.service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.manasa.inventoryservice.controller.Inventory;
import com.manasa.inventoryservice.repository.InventoryEntity;
import com.manasa.inventoryservice.repository.InventoryRepository;

import brave.Span;
import brave.Tracer;
import brave.Tracer.SpanInScope;

@Component
public class InventoryServiceImpl implements InventoryService {

	@Autowired
	private InventoryRepository inventoryRepository;

	@Autowired
	private Tracer tracer;

	/**
	 * Convert {@link Inventory} Object to {@link InventoryEntity} object Set
	 * the inventoryId if present else return object with id null/0
	 */
	private final Function<Inventory, InventoryEntity> inventoryToEntity = new Function<Inventory, InventoryEntity>() {
		@Override
		public InventoryEntity apply(Inventory inventory) {
			if (inventory.getId() == 0) {
				return new InventoryEntity(inventory.getProductCode(), inventory.getAvailableQuantity());
			} else {
				return new InventoryEntity(inventory.getId(), inventory.getProductCode(),
						inventory.getAvailableQuantity());
			}
		}
	};

	/**
	 * Convert {@link InventoryEntity} to {@link Inventory} object
	 */
	private final Function<InventoryEntity, Inventory> entityToInventory = new Function<InventoryEntity, Inventory>() {
		@Override
		public Inventory apply(InventoryEntity entity) {
			return new Inventory(entity.getId(), entity.getProductCode(), entity.getAvailableQuantity());
		}
	};

	@Override
	public Optional<Inventory> getInventoryById(Long inventoryId) {

		Span getInventoryByIdSpan = tracer.nextSpan().name("db::InventoryServiceImpl::getInventoryById").start();

		try (SpanInScope ws = tracer.withSpanInScope(getInventoryByIdSpan.start())) {
			return inventoryRepository.findById(inventoryId).map(i -> entityToInventory.apply(i));
		} finally {
			getInventoryByIdSpan.finish();
		}
	}

	@Override
	public List<Inventory> getAllInventories() {
		Span getAllInventoriesSpan = tracer.nextSpan().name("db::InventoryServiceImpl::getAllInventoriesSpan").start();

		try (SpanInScope ws = tracer.withSpanInScope(getAllInventoriesSpan.start())) {
			return inventoryRepository.findAll().parallelStream().map(i -> entityToInventory.apply(i))
					.collect(Collectors.toList());
		} finally {
			getAllInventoriesSpan.finish();
		}
	}

	@Override
	public boolean removeInventory(Long inventoryId) {
		Span removeInventorySpan = tracer.nextSpan().name("db::InventoryServiceImpl::removeInventorySpan").start();

		try (SpanInScope ws = tracer.withSpanInScope(removeInventorySpan.start())) {
			inventoryRepository.deleteById(inventoryId);
			return true;
		} finally {
			removeInventorySpan.finish();
		}
	}

	@Override
	public Optional<Inventory> saveUpdateInventory(Inventory inventory) {

		Span saveUpdateInventorySpan = tracer.nextSpan().name("db::InventoryServiceImpl::saveUpdateInventorySpan")
				.start();

		try (SpanInScope ws = tracer.withSpanInScope(saveUpdateInventorySpan.start())) {
			if (inventory.getId() == 0 || inventoryRepository.existsById(inventory.getId())) {
				InventoryEntity entity = inventoryRepository.save(inventoryToEntity.apply(inventory));
				return Optional.of(entityToInventory.apply(entity));
			} else {
				return Optional.empty();
			}
		} finally {
			saveUpdateInventorySpan.finish();
		}
	}

	public ResponseEntity<Inventory> findByProductCode(String productCode) {

		Span findByProductCodeSpan = tracer.nextSpan().name("db::InventoryServiceImpl::findByProductCodeSpan").start();
		Optional<InventoryEntity> inventoryItem = null;

		try (SpanInScope ws = tracer.withSpanInScope(findByProductCodeSpan.start())) {
			inventoryItem = inventoryRepository.findByProductCode(productCode);
		} finally {
			findByProductCodeSpan.finish();
		}

		if (inventoryItem.isPresent()) {
			return new ResponseEntity(entityToInventory.apply(inventoryItem.get()), HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}

}
