package com.manasa.inventoryservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.manasa.inventoryservice.controller.Inventory;

@Service
public interface InventoryService {

	/**
	 * 
	 * @param personId
	 * @return {@link Optional} {@link Inventory} objects if present in database
	 *         for supplied inventory ID
	 */
	public Optional<Inventory> getInventoryById(Long inventoryId);

	/**
	 * 
	 * @return {@link List} of {@link Inventory} model class for all available
	 *         entities
	 */
	public List<Inventory> getAllInventories();

	/**
	 * 
	 * @param inventoryId
	 * @return Delete the inventory from database for supplied inventory ID
	 */
	public boolean removeInventory(Long inventoryId);

	/**
	 * 
	 * @param inventory
	 * @return {@link Optional} {@link Inventory} objects after save or update
	 *         Save if no inventoryId present else update
	 */
	public Optional<Inventory> saveUpdateInventory(Inventory inventory);

	/**
	 * 
	 * @param productCode
	 * @return {@link Optional} {@link Inventory} objects if present in database
	 *         for supplied product code
	 */
	public ResponseEntity<Inventory> findByProductCode(String productCode);

}
