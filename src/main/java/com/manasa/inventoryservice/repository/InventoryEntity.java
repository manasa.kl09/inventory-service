package com.manasa.inventoryservice.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "inventory")
public class InventoryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "product_code", nullable = false, unique = true)
	private String productCode;
	
	@Column(name = "quantity")
	private Integer availableQuantity = 0;

	public InventoryEntity(String productCode, Integer availableQuantity) {
		this.productCode = productCode;
		this.availableQuantity = availableQuantity;
	}
	
	public InventoryEntity(Long id, String productCode, Integer availableQuantity) {
		this.id = id;
		this.productCode = productCode;
		this.availableQuantity = availableQuantity;
	}
}
